# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [3.0.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/compare/v2.0.0...v3.0.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* new feature in project.txt

### Features

*  new feature in project.txt ([f88f34c](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/commit/f88f34c68f1dca864f673927a276a93dfec277c2))

## [2.0.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/compare/v1.1.0...v2.0.0) (2021-07-21)


### ⚠ BREAKING CHANGES

* new feature in project.txt file

### Features

* new feature in project.txt file ([70ae45d](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/commit/70ae45d68eb1398064e8d698c7caebbda7eb3f10))

## [1.1.0](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/compare/v1.0.2...v1.1.0) (2021-07-21)


### Features

* new feature in project.txt file ([a1f978c](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/commit/a1f978cd05974a6845f6eb55b829318aef9ab150))

### [1.0.2](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/compare/v1.0.1...v1.0.2) (2021-07-21)


### Bug Fixes

* ignore package-lock.json ([b6b1736](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/commit/b6b1736d1f0ad386a821b91cfb6af3566564080d))

### [1.0.1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/compare/v1.0.0...v1.0.1) (2021-07-21)


### Bug Fixes

* fix gitignore ([598fcb1](https://gitlab.com/LibreFoodPantry/training/spikeathons/summer-2021/deploy-theas-pantry/auto-versioning-test/auto-versioning-example/commit/598fcb1f96b48b1cae738a60fe731401cbddb2b1))

## 1.0.0 (2021-07-21)
